import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register(){

	const {user} = useContext(UserContext);

    //State hooks to store the values of input fields
    const [FirstName, setFirstName] = useState('');   
    const [LastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [MobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(FirstName);
    console.log(LastName);
    console.log(email);
    console.log(MobileNo);
    console.log(password1);
    console.log(password2);

    function registerUser(event){

        // Prevents page redirection via form submission
        event.preventDefault();

        // Clear input fields
        setFirstName('');
        setLastName('');
        setEmail('');
        setMobileNo('');
        setPassword1('');
        setPassword2('');
        alert('Thank you for registering');
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
            
        }
        // Swal.fire({
        //             title: "Sorry Duplicate Email Found",
        //             icon: "error",
        //             text: "Try Another Email."
        //         });
        // //Navigate("/register");
    }, [email, password1, password2])

    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(event) => registerUser(event)}>
             <Form.Group controlId="FirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="FirstName" 
                    placeholder="Enter First Name" 
                    value = {FirstName}
                    onChange = {event => setFirstName(event.target.value)}
                    required
                />
                {/*<Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>*/}
            </Form.Group>   

            <Form.Group controlId="LastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="LastName" 
                    placeholder="Enter Last Name" 
                    value = {LastName}
                    onChange = {event => setLastName(event.target.value)}
                    required
                />
               
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="MobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="MobileNo" 
                    placeholder="Enter Mobile Number" 
                    value = {MobileNo}
                    onChange = {event => setMobileNo(event.target.value)}
                    required
                />
             
            </Form.Group>


            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </div>
        </Form>
    )
}